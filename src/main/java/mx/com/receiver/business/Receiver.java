package mx.com.receiver.business;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;


public class Receiver{

	 private static final Logger log = LoggerFactory.getLogger(Receiver.class);
	 private final static String QUEUE_NAME = "phones-queue";
	 @Autowired
	 ReceiverBO receiverBO;
	 
	 public Receiver() throws IOException, TimeoutException {
		 ConnectionFactory factory = new ConnectionFactory();
		 factory.setHost("localhost");
		 Connection connection = factory.newConnection();
		 Channel channel = connection.createChannel();
		 
		 channel.queueDeclare(QUEUE_NAME, false, false, false, null);
	 }
	 
	 @RabbitListener(queues=QUEUE_NAME)
	  public void receive(String message) {
		 log.info("Received message '{}'", message);
		 receiverBO.create(message);
	  }
}