package mx.com.receiver.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import mx.com.receiver.entity.AuditBean;
import mx.com.receiver.repository.AuditRepository;

@Component
public class ReceiverBO implements IReceiverBO{
	
	private static final Logger LOG = LoggerFactory.getLogger(Receiver.class);
	
	@Autowired
	AuditRepository auditRepository; 
	
	ObjectMapper mapper = new ObjectMapper();
		
	@Override
	public void create(String string){
		try {
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			AuditBean eventBean = mapper.readValue(string, AuditBean.class);
			auditRepository.save(eventBean);
		} catch (Exception e) {LOG.error(e.getMessage());}
		LOG.debug("Guardado en bitacora con exito");
	}

}
