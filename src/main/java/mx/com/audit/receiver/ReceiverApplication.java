package mx.com.audit.receiver;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

import mx.com.receiver.business.Receiver;

@SpringBootApplication
@ComponentScan({"mx.com.receiver.*"})
@EnableMongoRepositories("mx.com.receiver.repository")
@EnableRabbit
@EnableScheduling
@EnableDiscoveryClient
@EnableCircuitBreaker
@EnableAutoConfiguration(exclude = {
		AopAutoConfiguration.class
} )
public class ReceiverApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReceiverApplication.class, args);
	}
	
	@Bean
    public Receiver eventReceiver() throws IOException, TimeoutException {
      return new Receiver();
    }
}
