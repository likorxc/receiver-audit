package mx.com.receiver.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import mx.com.receiver.entity.AuditBean;

public interface AuditRepository extends MongoRepository <AuditBean, Object>{

}
