package mx.com.receiver.entity;

import java.io.Serializable;
import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;


@Document(collection = "audit_avl")
public @Data class AuditBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private ObjectId id;
	private Date createdAt = new Date();
	private String eventId;
	private String request;
	private String response;
	private boolean success;
	
}